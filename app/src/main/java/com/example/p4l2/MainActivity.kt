package com.example.p4l2

import android.app.job.JobInfo
import android.app.job.JobScheduler
import android.content.ComponentName
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    val jobId = 10

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        startJob.setOnClickListener { startJob() }
        cancelJob.setOnClickListener { cancelJob() }
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun startJob() {
        val serviceComponen = ComponentName(this, CuacaHariIni::class.java)
        val mJobInfo = JobInfo.Builder(jobId, serviceComponen)
            .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
            .setRequiresCharging(false)
            .setMinimumLatency(1 * 1000)
            .setOverrideDeadline(3 * 1000)

        val JobCuaca = getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler
        JobCuaca.schedule(mJobInfo.build())
        Toast.makeText(this, "Job Service Berjalan", Toast.LENGTH_SHORT)
            .show()
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun cancelJob() {
        val JobCuaca = getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler
        JobCuaca.cancel(jobId)
        Toast.makeText(this, "Job Service Berhenti", Toast.LENGTH_SHORT)
            .show()
    }
}
