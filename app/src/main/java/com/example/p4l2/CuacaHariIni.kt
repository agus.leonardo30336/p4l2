package com.example.p4l2

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.job.JobParameters
import android.app.job.JobService
import android.content.Context
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v4.app.NotificationCompat
import android.util.Log
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.AsyncHttpResponseHandler
import cz.msebera.android.httpclient.Header
import org.json.JSONObject
import java.text.DecimalFormat


@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
class CuacaHariIni : JobService() {
    val TAG = CuacaHariIni::class.java.simpleName
    val AppID = "63971c559501b72e05f66964fda3e3d2"
    val Kota = "Medan"

    override fun onStartJob(p0: JobParameters?): Boolean {
        Log.d(TAG, "Start Job")
        getCuacaHariIni(p0)
        return true
    }

    override fun onStopJob(p0: JobParameters?): Boolean {
        Log.d(TAG, "Stop Job")
        return true
    }

    private fun getCuacaHariIni(p0: JobParameters?) {
        val client = AsyncHttpClient()
        val url = "http://api.openweathermap.org/data/2.5/weather?q=$Kota&APPID=$AppID"

        val charset = Charsets.UTF_8

        val handler = object : AsyncHttpResponseHandler() {
            override fun onSuccess(
                statusCode: Int, headers: Array<out Header>?,
                responseBody: ByteArray?
            ) {
                var result = ""
                if (responseBody != null) {
                    result = responseBody.toString(charset)
                    val obj = JSONObject(result)
                    val weatherArray = obj.getJSONArray("weather")
                    val weather = weatherArray.getJSONObject(0)
                    val weatherMain = weather.getString("main")
                    val weatherDescription = weather.getString("description")

                    val main = obj.getJSONObject("main")
                    val tempMin = main.getDouble("temp_min") - 274.15
                    val tempMax = main.getDouble("temp_max") - 274.15

                    val df = DecimalFormat("0.##")

                    val mBuilder = NotificationCompat.Builder(this@CuacaHariIni, "asd")
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("Cuaca Hari ini")
                        .setContentText(weatherMain)
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                        .setStyle(
                            NotificationCompat.BigTextStyle()
                                .bigText("\nDeskripsi : ${weatherDescription.capitalize()}\nSuhu : ${df.format(tempMin)} - ${df.format(tempMax)}")
                        )

                    val notificationManager: NotificationManager =
                        getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                    val id = 30103

                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                        val channelId = "asd"
                        val channelName = "test"
                        val importance = NotificationManager.IMPORTANCE_LOW

                        val mChannel = NotificationChannel(
                            channelId, channelName, importance)
                        notificationManager.createNotificationChannel(mChannel)
                    }

                    notificationManager.notify(id, mBuilder.build())
                }
                Log.d(TAG, result)
                jobFinished(p0, false)
            }

            override fun onFailure(
                statusCode: Int, headers: Array<out Header>?,
                responseBody: ByteArray?, error: Throwable?
            ) {
                jobFinished(p0, true)
                Log.d(TAG, "Faild")
            }
        }
        client.get(url, handler)
    }
}